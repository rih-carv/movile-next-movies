//
//  StringConsumer.swift
//  Movies
//
//  Created by Ricardo Carvalho on 17/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import Foundation

protocol StringConsumer {
    func consume(_ string: String)
}

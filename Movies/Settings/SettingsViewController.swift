//
//  SettingsViewController.swift
//  Movies
//
//  Created by Ricardo Carvalho on 16/11/18.
//  Copyright © 2018 Ricardo Carvalho. All rights reserved.
//

import UIKit

private enum SettingsKeys {
    static let theme = "theme"
    static let autoPlay = "autoPlay"
}

class SettingsViewController: UIViewController {
    private let defaults = UserDefaults.standard
    
    @IBOutlet weak var themeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var autoPlaySwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        themeSegmentedControl.selectedSegmentIndex = defaults.integer(forKey: SettingsKeys.theme)
        autoPlaySwitch.isOn = defaults.bool(forKey: SettingsKeys.theme)
    }
    
    @IBAction func themeDidChangeValue(_ sender: UISegmentedControl) {
        defaults.set(sender.selectedSegmentIndex, forKey: SettingsKeys.theme)
        defaults.synchronize()
    }
    
    @IBAction func autoPlayDidChangeValue(_ sender: UISwitch) {
        defaults.set(sender.isOn, forKey: SettingsKeys.autoPlay)
        defaults.synchronize()
    }
}
